# vocabularies

Controlled vocabularies for permafrost data. [See the full list here](https://permafrostnet.gitlab.io/vocabularies/index.html)

## Other representations

Vocabularies can be displayed as JSON or TTL.  Just change the extension on the url:
* [https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.html](https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.html) (HTML)
* [https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.ttl](https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.ttl) (TTL)
* [https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.json](https://permafrostnet.gitlab.io/vocabularies/vocabs/landforms/CPERSLF.json) (JSON)

## Suggesting new vocabulary entries or modifications
To suggest a new vocabulary entry or a modification to an existing entry, [create an issue here](https://gitlab.com/permafrostnet/vocabularies/-/issues/new).  You will need to register for a free GitLab account.

 
